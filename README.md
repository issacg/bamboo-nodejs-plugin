# Node.js Bamboo Plugin

Plugin for integrating [Node.js][node] with [Atlassian Bamboo][bamboo].

[node]: http://nodejs.org/
[bamboo]: http://www.atlassian.com/software/bamboo/overview

## Tasks

* node
* npm
* Mocha test parsing task

## To do/ideas

* grunt task
* nodeunit test parsing task

## Integrating Mocha & Bamboo with mocha-bamboo-reporter

In your package.json file, add a devDependency on "[mocha-bamboo-reporter](https://npmjs.org/package/mocha-bamboo-reporter)", and a script "bamboo" as outlined below...

    package.json
    
    ...
    "devDependencies": {
        ...
        "mocha": ">=1.8.1",
        "mocha-bamboo-reporter": "*"
    }
    
    "scripts": {
        ...
        "bamboo": "node node_modules/mocha/bin/mocha -R mocha-bamboo-reporter"
    }
    
In Bamboo:

* add a "npm" task with command `run-script bamboo` which will create an output file (named "mocha.json")
* add a "Parse Mocha results" task which runs afterwards to parse the test results

If you don't do a full checkout on each build, make sure you add a task to delete mocha.json BEFORE the `npm run-script bamboo` task (a simple script task that runs `rm -f mocha.json` should do the trick)

### Alternatively

To run your Mocha tests without modifying your package.json you can simply do the following in Bamboo:

* add a "npm" task with command `install mocha-bamboo-reporter`
* add a "Node.js" task with script `node_modules/mocha/bin/mocha` and arguments `--reporter mocha-bamboo-reporter`, along with any other arguments you want to pass to Mocha
* add a "Parse Mocha results" task to parse the test results

## License

Copyright (C) 2013 - 2013 Atlassian Corporation Pty Ltd. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.