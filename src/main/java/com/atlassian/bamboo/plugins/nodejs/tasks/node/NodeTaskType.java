package com.atlassian.bamboo.plugins.nodejs.tasks.node;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeConfigurator;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class NodeTaskType implements TaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String NODE_CAPABILITY_PREFIX = CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".node";
    public static final String NODE_PATH = File.separatorChar + "node" + (SystemUtils.IS_OS_WINDOWS ? ".exe" : "");
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final CapabilityContext capabilityContext;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public NodeTaskType(ProcessService processService, CapabilityContext capabilityContext)
    {
        this.processService = processService;
        this.capabilityContext = capabilityContext;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods

    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);

        try
        {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();
            final String nodePath = Preconditions.checkNotNull(capabilityContext.getCapabilityValue(NodeTaskType.NODE_CAPABILITY_PREFIX + "." + configurationMap.get(AbstractNodeConfigurator.RUNTIME)), "Builder path is not defined");
            final String command = configurationMap.get(AbstractNodeConfigurator.COMMAND);
            final String arguments = configurationMap.get(NodeConfigurator.ARGUMENTS);

            final List<String> commandsList = Lists.newArrayList(nodePath);
            commandsList.addAll(CommandlineStringUtils.tokeniseCommandline(command));

            if (StringUtils.isNotBlank(arguments))
            {
                commandsList.addAll(CommandlineStringUtils.tokeniseCommandline(arguments));
            }

            taskResultBuilder.checkReturnCode(processService.executeProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandsList)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }

    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
