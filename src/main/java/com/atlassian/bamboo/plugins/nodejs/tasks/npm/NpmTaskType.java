package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeTaskType;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;

public class NpmTaskType implements TaskType
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String NPM_PATH = File.separatorChar + "npm" + (SystemUtils.IS_OS_WINDOWS ? ".cmd" : "");
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProcessService processService;
    private final CapabilityContext capabilityContext;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public NpmTaskType(final ProcessService processService, final CapabilityContext capabilityContext)
    {
        this.processService = processService;
        this.capabilityContext = capabilityContext;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext) throws TaskException
    {
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);

        try
        {
            final ConfigurationMap configurationMap = taskContext.getConfigurationMap();
            final String nodePath = Preconditions.checkNotNull(capabilityContext.getCapabilityValue(NodeTaskType.NODE_CAPABILITY_PREFIX + "." + configurationMap.get(AbstractNodeConfigurator.RUNTIME)), "Builder path is not defined");
            final String npmPath = replaceLast(nodePath, NodeTaskType.NODE_PATH, NPM_PATH);
            final String command = configurationMap.get(AbstractNodeConfigurator.COMMAND);
            
            final List<String> arguments = CommandlineStringUtils.tokeniseCommandline(command);
            List<String> commandsList = Lists.newArrayList(npmPath);
            commandsList.addAll(arguments);

            taskResultBuilder.checkReturnCode(processService.executeProcess(taskContext, new ExternalProcessBuilder()
                    .command(commandsList)
                    .workingDirectory(taskContext.getWorkingDirectory())));

            return taskResultBuilder.build();
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }

    }
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * Horrible, slow replaceLast implementation
     * @param input string to search
     * @param find string to match
     * @param replace string to replace last match with
     * @return string with last match replaced
     */
    static String replaceLast(final String input, final String find, final String replace)
    {
        String output = StringUtils.reverse(sanitiseString(input)).replaceFirst(
                StringUtils.reverse(sanitiseString(find)),
                StringUtils.reverse(sanitiseString(replace))
        );
        return dirtyString(StringUtils.reverse(output));
    }
    private static String sanitiseString(final String input)
    {
        return input.replaceAll("\\\\", "\\\\\\\\");
    }
    private static String dirtyString(final String input)
    {
        return input.replaceAll("\\\\\\\\", "\\\\");
    }
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
