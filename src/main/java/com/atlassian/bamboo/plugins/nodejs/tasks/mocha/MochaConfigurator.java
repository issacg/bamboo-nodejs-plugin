package com.atlassian.bamboo.plugins.nodejs.tasks.mocha;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.BuildTaskRequirementSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import org.apache.commons.lang.StringUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.opensymphony.xwork.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Map;
import java.util.Set;

public class MochaConfigurator extends AbstractTaskConfigurator implements BuildTaskRequirementSupport
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String PATTERN = "testPattern";
    public static final String CTX_UI_CONFIG_BEAN = "uiConfigBean";
    protected static final Set<String> FIELDS_TO_COPY = Sets.newHashSet();

    static {
        FIELDS_TO_COPY.add(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY);
        FIELDS_TO_COPY.add(PATTERN);
    }

    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    public TextProvider textProvider;
    public UIConfigSupport uiConfigSupport;


    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
        if (StringUtils.isEmpty(params.getString(PATTERN)))
        {
            errorCollection.addError(PATTERN, textProvider.getText("mocha.pattern.error"));
        }
    }
    
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> map = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(map, params, FIELDS_TO_COPY);
        return map;
    }
    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        populateContextForAllOperations(context);
        context.put(PATTERN, "mocha.json");
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAllOperations(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    public void populateContextForAllOperations(@NotNull Map<String, Object> context)
    {
        context.put(CTX_UI_CONFIG_BEAN, uiConfigSupport);
    }

    public void setTextProvider(TextProvider textProvider)
    {
        this.textProvider = textProvider;
    }

    public void setUiConfigSupport(UIConfigSupport uiConfigSupport)
    {
        this.uiConfigSupport = uiConfigSupport;
    }
}
