package com.atlassian.bamboo.plugins.nodejs.tasks.mocha;

import java.util.List;

public class MochaSuite {
	MochaSuiteStats stats;
	List<MochaSuiteTest> skipped;
	List<MochaSuiteTest> failures;
	List<MochaSuiteTest> passes;
	
	@Override
	public String toString() {
		return "Mocha suite of " + Integer.toString(stats.tests) + " tests";
	}
}
