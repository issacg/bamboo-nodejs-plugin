package com.atlassian.bamboo.plugins.nodejs.tasks.mocha;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.io.BufferedReader;
import java.io.FileReader;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.google.gson.*;

public class MochaReportCollector implements TestReportCollector {
	private static final Logger log = Logger.getLogger(MochaReportCollector.class);
	@Override
	@NotNull
	public TestCollectionResult collect(@NotNull File file) throws Exception {
		Collection<TestResults> pass = Lists.newArrayList();
		Collection<TestResults> fail = Lists.newArrayList();
		Collection<TestResults> skip = Lists.newArrayList();
		String json = "";
		try {
		    BufferedReader br = new BufferedReader(new FileReader(file));
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
	
		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		        json = sb.toString();
		    } finally {
		        br.close();
		    }
		} catch (Exception ex) {}
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
		MochaSuite results = gson.fromJson(json, MochaSuite.class);

		for (MochaSuiteTest test : results.passes) {
			TestResults testResult = new TestResults(test.fullTitle, test.title, String.valueOf(test.duration / 1000));
			testResult.setState(TestState.SUCCESS);
			pass.add(testResult);
		}
		for (MochaSuiteTest test : results.failures) {
			TestResults testResult = new TestResults(test.fullTitle, test.title, String.valueOf(test.duration / 1000));
			testResult.setState(TestState.FAILED);
			testResult.addError(new TestCaseResultErrorImpl(test.error));
			fail.add(testResult);
		}
		for (MochaSuiteTest test : results.skipped) {
			TestResults testResult = new TestResults(test.fullTitle, test.title, String.valueOf(test.duration / 1000));
			testResult.setState(TestState.SKIPPED);
			skip.add(testResult);
		}
		
		TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
		log.info(results.toString());
		log.info(pass.size() + " tests passed.");
		log.info(skip.size() + " tests skipped.");
		log.info(fail.size() + " tests failed.");
		builder.addSuccessfulTestResults(pass);
		builder.addFailedTestResults(fail);
		builder.addSkippedTestResults(skip);
		return builder.build();
	}

	@Override
	@NotNull
	public Set<String> getSupportedFileExtensions() {
		return Sets.newHashSet("json");
	}
}
