package com.atlassian.bamboo.plugins.nodejs;

import com.atlassian.bamboo.v2.build.agent.capability.AbstractFileCapabilityDefaultsHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityDefaultsHelper;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class NodeCapabilityDefaultsHelper extends AbstractFileCapabilityDefaultsHelper
{
    private static final Logger log = Logger.getLogger(NodeCapabilityDefaultsHelper.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String NODE_EXEC_NAME = "node";
    private static final String NODE = "Node.js";
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @NotNull
    @Override
    protected String getExecutableName()
    {
        return SystemUtils.IS_OS_WINDOWS ? NODE_EXEC_NAME + ".exe" : NODE_EXEC_NAME;
    }

    @NotNull
    @Override
    protected String getCapabilityKey()
    {
        return CapabilityDefaultsHelper.CAPABILITY_BUILDER_PREFIX + ".node" + "." + NODE;
    }
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
