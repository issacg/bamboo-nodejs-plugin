package com.atlassian.bamboo.plugins.nodejs.tasks.node;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

public class NodeConfigurator extends AbstractNodeConfigurator
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String ARGUMENTS = "arguments";

    static {
        FIELDS_TO_COPY.add(ARGUMENTS);
    }
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        if (StringUtils.isEmpty(params.getString(AbstractNodeConfigurator.RUNTIME)))
        {
            errorCollection.addError(AbstractNodeConfigurator.RUNTIME, super.textProvider.getText("node.runtime.error"));
        }
        if (StringUtils.isEmpty(params.getString(AbstractNodeConfigurator.COMMAND)))
        {
            errorCollection.addError(AbstractNodeConfigurator.COMMAND, super.textProvider.getText("node.command.error"));
        }
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
