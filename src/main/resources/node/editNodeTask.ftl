[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='node' /][/#assign]
[@ww.select cssClass="builderSelectWidget" labelKey='node.runtime' name='runtime'
list=uiConfigBean.getExecutableLabels('node')
extraUtility=addExecutableLink required=true /]

[@ww.textfield labelKey='node.command' name='command' required=true /]
[@ww.textfield labelKey='node.arguments' name='arguments' /]
[@ww.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]